
1.  
![test0](img/test0.jpg), 
![test1](img/test1.gif), 
![test2](img/test2.png), 
[file3.txt](file3.txt), 

---

2. Changing image size:   
    a) <img src="img/test0.jpg" width="100" height="80"></img>, 

    b) ![](img/test0.jpg){width=50px},  
    
![](img/test0.jpg){.css}  

![](img/test0.jpg){:.css}  

![](img/test0.jpg){: .css}  


<style type="text/css">
.css{  
    zoom: 50%;
    
}
</style>


    c) 
<img src="img/test0.jpg" width="50%" height="80%" ></img>, 
    
    d) 
<img src="img/test0.jpg" style="width:50%; height:80%" ></img>, 




----
![test0](./img/test0.jpg), 
![test0](<./img/test0.jpg>), 

